﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace BangLib.Wpf.Converter {

    public class ListToStringConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return null;
            if (!(value is IEnumerable<object> list))
                return null;
            Collection<object> result = new Collection<object>();
            for (int i = 0, loopTo = list.Count() - 1; i <= loopTo; i++) {
                object item = list.ElementAtOrDefault(i);
                if (item.GetType() == typeof(string)) {
                    foreach (string lineCrLf in item.ToString().Split("\r\n".ToCharArray())) {
                        foreach (string lineCr in lineCrLf.Split('\r')) {
                            foreach (string lineLf in lineCr.Split('\n')) {
                                if (!string.IsNullOrEmpty(lineLf) && !string.IsNullOrWhiteSpace(lineLf))
                                    result.Add(lineLf);
                            }
                        }
                    }
                } else if (typeof(IEnumerable<object>).IsAssignableFrom(item.GetType()) && ((ICollection)item).Count > 0)
                    result.Add(Convert(item, targetType, parameter, culture)); // recursion
                else
                    result.Add(item);
            }
            return string.Join((parameter != null ? ", " : Environment.NewLine).ToString(), result.ToArray());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BangLib.Wpf.Converter {

    public class NullToTypeConverter : IValueConverter {

        /// <summary>
        ///
        ///  NullToTypeConverter is a Multi-Purpose Converter, acting different depending on the bound target Type.
        ///  - if the target type is a bool, it will return true if value not null/empty/0, otherwise false.
        ///  - if the target type is a string, it will return the converted value if not null/empty/0, otherwise null.
        ///  - if the target type is a Visibility, it will return Visible if not null/empty/0, otherwise Collapsed.
        ///
        ///  null checks are implemented for:
        ///  - objects (null)
        ///  - bools (null or false)
        ///  - strings (null or empty)
        ///  - ints, doubles (null or 0 or 0.0)
        ///  - enumerables, collections, lists (null or empty)
        ///
        ///  use ConverterParameter for return value inversion (True) or string formatting ({}*{0}*).
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter">set to "True" to invert result, or set to any StringFormat to apply formatting only if not null/empty/0</param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            bool result = true;

            if (value == null || // null object
                value.GetType() == typeof(bool) && !(bool)value || // false bool
                value.GetType() == typeof(string) && (string.IsNullOrEmpty(value.ToString()) || string.IsNullOrWhiteSpace(value.ToString())) || // empty string
                (value.GetType() == typeof(int) || value.GetType().IsEnum) && (int)value == 0 || // 0 int/enum
                value.GetType() == typeof(double) && (double)value == 0.0 || // 0 double
                typeof(IEnumerable<object>).IsAssignableFrom(value.GetType()) && ((ICollection)value).Count == 0 ||  // empty enumerable/collection
                typeof(IList<object>).IsAssignableFrom(value.GetType()) && ((IList)value).Count == 0) // empty list
                result = false;

            if (parameter != null) {
                string param = parameter.ToString();
                if (param == true.ToString()) // invert?
                    result = !result;
                else if (result && param.Contains("{0}")) // StringFormat
                    return param.Replace("{0}", value.ToString());
            }

            if (targetType == typeof(bool))
                return result;
            else if (targetType == typeof(string))
                return result ? value.ToString() : string.Empty;
            else if (targetType == typeof(Visibility))
                return result ? Visibility.Visible : Visibility.Collapsed;
            else
                return result ? value : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Windows;
using System.Windows.Media;

namespace BangLib.Wpf.ViewModels {

    public class ClosableTabItemBinding : ViewModelBase {
        private string _Id;

        private string _Header;

        private Guid _ClosingGuid = Guid.NewGuid();

        private bool _Closable = true;

        private FrameworkElement _Content;

        private Brush _Background;

        public string Id {
            get {
                return _Id;
            }
            set {
                SetProperty(ref _Id, value);
            }
        }

        public FrameworkElement Content {
            get {
                return _Content;
            }
            set {
                SetProperty(ref _Content, value);
            }
        }

        public string Header {
            get {
                return _Header;
            }
            set {
                SetProperty(ref _Header, value);
            }
        }

        public Guid ClosingGuid {
            get {
                return _ClosingGuid;
            }
        }

        public bool Closable {
            get {
                return _Closable;
            }
            set {
                SetProperty(ref _Closable, value);
            }
        }

        public Visibility CloseButtonVisibility {
            get {
                return _Closable ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Brush Background {
            get {
                return _Background;
            }
            set {
                SetProperty(ref _Background, value);
            }
        }
    }
}